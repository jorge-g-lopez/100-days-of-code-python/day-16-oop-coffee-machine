python3 -m isort -v $1
python3 -m black $1
python3 -m flake8 -v --show-source --benchmark $1
python3 -m pylint $1
