"""OOP Coffee Machine"""

from coffee_maker import CoffeeMaker
from menu import Menu, MenuItem
from money_machine import MoneyMachine

QUESTION = "What would you like? (" + Menu().get_items() + "): "
on = True
mycoffeemaker = CoffeeMaker()
mymoneymachine = MoneyMachine()

while on:
    beverage = input(QUESTION).lower()
    if beverage == "off":
        on = False
    elif beverage == "report":
        mycoffeemaker.report()
        mymoneymachine.report()
    else:
        drink = Menu().find_drink(beverage)
        if drink is not None:
            if mycoffeemaker.is_resource_sufficient(drink):
                if mymoneymachine.make_payment(drink.cost):
                    mycoffeemaker.make_coffee(drink)
